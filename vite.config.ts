import { defineConfig } from 'vite'
import svgrPlugin from 'vite-plugin-svgr'
import tsconfigPaths from 'vite-tsconfig-paths'
import react from '@vitejs/plugin-react'
import checker from 'vite-plugin-checker'


// https://vitejs.dev/config/
export default defineConfig({
  // This changes the out put dir from dist to build
  // comment this out if that isn't relevant for your project
  build: {
    outDir: 'build',
    // commonjsOptions: {
    //     include: [/src\/core\/services\/rasmik-client\/index.js/, /node_modules/]
    //   }
  },
  server: {
    port: +(process.env as any).PORT,
  },
  plugins: [
    react({
      fastRefresh:true,
      babel:{
        parserOpts: {
          plugins: ['decorators-legacy', 'classProperties']
        }
      }
    }),
    tsconfigPaths(),
    // reactRefresh(), //already covered by react() plugin
    svgrPlugin({
      svgrOptions: {
        icon: true,
        // ...svgr options (https://react-svgr.com/docs/options/)
      },
    }),
    checker({
      overlay:true,
      typescript:true,
      terminal:false,
    })
  ],
})
