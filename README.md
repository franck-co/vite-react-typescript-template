# vite react template typescript


## setup
### Install dependencies
```
yarn install
```
### configure your envirmonment

Add a .env.development file to the root of your project. Make sure that their is at list the following environment variables :
```
PUBLIC_URL=http://localhost:4000
VITE_API_URL=http://monbackenddedev.com
PORT=4000
VITE_EDITOR=code
EDITOR=code
```

<br>


## scripts

`yarn start` to start the development server

`yarn build` to generate the production build (environment must be set via env-cmd or a dockerfile)

`yarn clear-cache` to clear vite cache if it gets corrupted

<br>

## debug
1. Ensure the development server is started
2. Goto the debug panel (Ctrl + shift + D)
3. Select "debug:4000" and click play to open your website in a chrome tab that is linked to vscode