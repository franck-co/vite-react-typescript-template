FROM node:lts-alpine AS builder

WORKDIR /app
COPY . .

ENV NODE_ENV=production

#Define env variables for production here
ENV PUBLIC_URL=https://mysite.com
ENV VITE_API_URL=https://mybackend.fr

#skip linting for faster deployment
ENV DISABLE_ESLINT_PLUGIN=true

RUN yarn install --production
RUN yarn build

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/build .
COPY nginx.conf /etc/nginx/conf.d/default.conf
ENTRYPOINT ["nginx", "-g", "daemon off;"]
