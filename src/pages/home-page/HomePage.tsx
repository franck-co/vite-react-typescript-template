import React, { useCallback, useEffect } from "react";
import { Box, Button } from '@mui/material'

import { Center, HFlex, PageContent } from 'core/atoms';
import { useDialogClassic } from 'core/hooks';
import { DemoDialog } from './components/DemoDialog';
import { useNavigate } from 'react-router-dom';


export function HomePage() {

  const demoDialogLogic = useDialogClassic()

  const handleDialogSuccess = useCallback((response: string) => {
    alert(response)
  }, [])

  const handleDialogCancel = useCallback(() => {
    alert('dialog cancelled')
  }, [])

  const nevigate = useNavigate()


  return (
    <PageContent>

      <Center flexGrow={1} gap="10px">
        <Button variant="outlined" onClick={() => demoDialogLogic.open()}>Ouvrir dialog</Button>
        <Button variant="outlined" onClick={() => nevigate('../playground')}>Playground</Button>
      </Center>

      {demoDialogLogic.isOpen && <DemoDialog hide={demoDialogLogic.hide} onSuccess={handleDialogSuccess} onCancel={handleDialogCancel} />}

    </PageContent>
  );
}