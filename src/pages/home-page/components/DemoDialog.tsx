import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@mui/material';
import { Center, HFlex, Spacer } from 'core/atoms';
import { DialogClassicProps } from 'core/hooks';
import { useCallback, useState } from 'react';


interface DemoDialogProps  extends DialogClassicProps {
    onSuccess:(enteredText:string)=>any
    onCancel: () => any
}
export function DemoDialog(props: DemoDialogProps){
    const { hide, onSuccess, onCancel } = props

    const [textState, setTextState] = useState("")

    const handleTextChange = useCallback((newText:string)=>{
        setTextState(newText)  
    },[])

    const handleCancelClick = useCallback(()=>{
        hide();
        onCancel();
    }, [])

    const handleSuccessClick = useCallback(()=>{
        hide();
        onSuccess(textState);
    },[textState])

    return (
        <Dialog open fullWidth maxWidth="sm">
            <DialogTitle>Choisissez une couleur</DialogTitle>
            <DialogContent>
                <Center height={"100px"}>
                    <TextField
                        fullWidth
                        label="Entrez du texte"
                        onChange={event => handleTextChange(event.target.value)}
                    />
                </Center>
            </DialogContent>
            <DialogActions>
                <HFlex>
                    <Button onClick={handleCancelClick}>Annuler</Button>
                    <Spacer/>
                    <Button disabled={textState.length === 0} onClick={handleSuccessClick} variant="contained">Valider</Button>
                </HFlex>
            </DialogActions>
        </Dialog>
    )
}