import { Box } from '@mui/system'
import { Center, Pie } from 'core/atoms'

export function Exemple() {
    return (
        <Center height={"100%"} padding="50px">
            <Pie colors={['magenta', 'cyan', 'yellow']} smooth style={{ aspectRatio: "1 / 1", height: "100%" }}> <div>Hello playground !</div></Pie>
        </Center>
    )
}