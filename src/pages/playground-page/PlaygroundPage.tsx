import { Button, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { Center, HFlex, PageContent, StyledLink } from 'core/atoms';
import { useCallback } from 'react';
import { useNavigate } from 'react-router-dom'
import { Exemple } from './Example';

export function PlaygroundPage() {


    const navigate = useNavigate()

    const handleNavigateToHomeClick = useCallback(() => {
        navigate('../home')
    }, [])

    //3 ways to navigate to home
    return (
        <PageContent>

            <Box>
                <Center><Typography>4 façons de naviguer</Typography></Center>
                <HFlex justifyContent="space-around">
                    <StyledLink to={"../home"} >Home</StyledLink>
                    <StyledLink to={"/home"} >Home</StyledLink>
                    <Button onClick={handleNavigateToHomeClick} >Home</Button>
                    <Button onClick={() => navigate('../home')} > Home</Button >
                </HFlex>
            </Box>

            <Box flexGrow={1}>
                <Exemple />
            </Box>
         
        </PageContent >
    )
}