type Nullable<T> = T | null;
type FreeObj<T = void> = T  extends void ? Record<string | symbol,any> : Record<string | symbol,any> & T

type nstring = null | string
type nnumber = null | number
type NDate = null | Date

interface Window { 
    [key:string]:any;
    unlayer: any;
 }


type Constructor<T> = new (...args : any[]) => T;
type AbstractConstructor<T> = new (...args : any[]) => T;
