import React from 'react';
import ReactDOM from 'react-dom/client';
import './other/css/global.css';

import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import * as themes from './other/themes'

import { BrowserRouter } from 'react-router-dom'

import { ConditionalWrapper } from './core/utils/react'

import {
  QueryClient,
  QueryClientProvider,
} from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
// import { persistQueryClient } from 'react-query/persistQueryClient-experimental'
//  import { createWebStoragePersistor } from 'react-query/createWebStoragePersistor-experimental'
 
import { Settings } from 'luxon';
import { App } from 'core/app/App';
const isStrictMode = false// || true;

Settings.defaultLocale = "fr";


/* React query settings */
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      cacheTime: 1000 * 60 * 15, // 15 minutes
    },
  },
})


////To persist the query cache across tabs
// const localStoragePersistor = createWebStoragePersistor({storage: window.localStorage, deserialize:parseWithDate})

// persistQueryClient({
//   queryClient,
//   persistor: localStoragePersistor,
// })


ReactDOM.createRoot(document.getElementById('root')!).render(


  <ThemeProvider theme={themes.custom1}>
    <CssBaseline />

    <ConditionalWrapper condition={isStrictMode} wrapper={(children: any) => <React.StrictMode>{children}</React.StrictMode>}>
    <QueryClientProvider client={queryClient}>

      <BrowserRouter>
          <App />
      </BrowserRouter>

      <ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
      </QueryClientProvider>
    </ConditionalWrapper>

  </ThemeProvider>
);



declare global {
    interface ImportMetaEnv{
      PORT?: string;
      VITE_API_URL:string
      SKIP_PREFLIGHT_CHECK:string
      VITE_EDITOR:string
    }

}
