import { Box, BoxProps } from '@mui/material';


export function PageContent(props: BoxProps & { children: BoxProps['children'] | BoxProps['children'][] }) {

    return <Box display="flex" flexDirection="column" flexGrow={1} height="100%"  {...props} />

}