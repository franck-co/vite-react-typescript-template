import { ChipProps, ButtonProps, Box, Chip, Button } from "@mui/material"

export interface ChipWithButtonProps extends ChipProps {
    buttonLabel: string
    onButtonClick?: () => any
    buttonProps?: ButtonProps
}

export function ChipWithButton(props: ChipWithButtonProps) {

    const { buttonLabel, buttonProps, onButtonClick, ...rest } = props

    return (
        <Box>
            <Box display="block" textAlign="center">
                <Chip {...rest} />
            </Box>
            <Box display="block" textAlign="center">
                <Button {...buttonProps} onClick={onButtonClick} style={{ fontSize: "x-small", ...buttonProps?.style }}>{buttonLabel}</Button>
            </Box>
        </Box>
    )
}