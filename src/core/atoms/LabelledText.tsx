import { TypographyProps, BoxProps, Box, Typography } from '@mui/material'
import React, { ReactNode } from 'react'



export interface LabelledTextProps {
    label: string
    children: ReactNode
    _label?: TypographyProps
    _text?: TypographyProps
    _container?: BoxProps
}

export function LabelledText(props: LabelledTextProps) {
    const { _label, _text, _container, children, label } = props

    return (
        <Box {..._container}>
            <Typography variant="subtitle2" color="GrayText" {..._label}>{label}</Typography>
            <Typography {..._text}>{children}</Typography>
        </Box>
    )
}
