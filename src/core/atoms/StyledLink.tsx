
import { styled } from '@mui/material'
import { HTMLAttributeAnchorTarget, ReactNode } from 'react'
import { Link as RRLink, LinkProps} from 'react-router-dom'


const StyledRRLink = styled(RRLink)({
    'color': 'inherit',
    ':hover': { color: 'black' },
    ':visited': { color: '#985f13' },
})

const StyledA = styled('a')({
    'color': 'inherit',
    ':hover': { color: 'black' },
    ':visited': { color: '#985f13' },
})

export function StyledLink(props: Partial<LinkProps> & {url?:string,target?:HTMLAttributeAnchorTarget}){
    if(props.url){
        return <StyledA href={props.url} target={props.target}>{props.children}</StyledA>
    }else {
        return <StyledRRLink {...props as LinkProps } />
    }
}