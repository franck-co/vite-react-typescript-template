import { FileCopy as FileCopyIcon } from "@mui/icons-material";
import {
    IconButton,
  } from '@mui/material'
import { copyToClipboard } from 'core/utils';

export function CopyButton({text}:{text:string}){
    return (
      <IconButton
        color="primary"
        aria-label="copy text"
        component="span"
        onClick={()=>copyToClipboard(text)}
        size="large">
            <FileCopyIcon />
          </IconButton>
    );
  }