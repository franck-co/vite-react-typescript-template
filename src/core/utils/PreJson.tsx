import { useFormikContext } from 'formik'
import { CSSProperties } from 'react'

export function PreJson({ data, indent = 2, style }: { data: any, indent?: number ,style?:CSSProperties }) {
    return (<pre style={{padding:'5px',...style}}>{JSON.stringify(data, null, indent)}</pre>)
}

export function PreFmkValues({ indent = 2, style }: { indent?: number,style?:CSSProperties  }) {
    const { values, errors } = useFormikContext()
    if(import.meta.env.PROD) return null
    return (<PreJson data={{...values as object,errors}} indent={indent} />)
} 