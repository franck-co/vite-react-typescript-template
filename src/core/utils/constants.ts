export const STATUS_COLORS = {
    DRAFT: 'blue',
    READY: 'cyan',
    PENDING: 'orange',
    OK: 'green',
    URGENT: 'pink',
    DEAD: 'black',
    DISABLED: 'gray',
    ERROR: 'red'
} as const
