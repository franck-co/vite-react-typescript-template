// import { useHistory, useLocation } from 'react-router';
import { useNavigate, useLocation } from 'react-router';

export const copyToClipboard = (text: string) => navigator.clipboard.writeText(text);

export function hasSameMonthYear(isoDateString1: string, isoDateString2: string) {
  if (!isoDateString1 || !isoDateString2) return false
  return (isoDateString1.substr(0, 7) === isoDateString2.substr(0, 7))
}