import { DateTime, Duration } from 'luxon';

export function timeStringToMinutes(isoTimeString: string) {
  return Duration.fromISOTime(isoTimeString).as('minutes')
}

export function percentageToColor(percentage: number, maxHue = 120, minHue = 0) {
  const hue = percentage * (maxHue - minHue) + minHue;
  return `hsl(${hue}, 100%, 50%)`;
}


export function booleanToString(val: boolean | null) {
  if (val === true) return 'true'
  if (val === false) return 'false'
  if (val === null) return 'null'
}

export function stringToBoolean(val: string) {
  if (val === 'true') return true
  if (val === 'false') return false
  if (val === 'null') return null
}
