import { isNil } from 'lodash'
import { useState, useCallback, useRef } from 'react'


export function useDialogClassic<Payload = void>() {
    const [isOpen, setOpened] = useState(false)
    const [payload, setPayload] = useState<Payload | null>(null)
    const open = useCallback((payload?: Payload) => { setOpened(true); payload && setPayload(payload) }, [])
    const hide = useCallback(() => { setOpened(false);setPayload(null) }, [])
    return {
        isOpen,
        open,
        hide,
        payload
    }
}

export interface DialogClassicProps {
    hide: () => any
}


export function useDialogId<Payload = void>() {
    const [id, setId] = useState<number | null>(null)
    const [payload, setPayload] = useState<Payload | null>(null)
    const open = useCallback((id:number,payload?: Payload) => { setId(id); payload && setPayload(payload) }, [])
    const hide = useCallback(() => { setId(null);setPayload(null) }, [])
    return {
        isOpen:!isNil(id),
        id,
        open,
        hide,
        payload
    }
}



const createSym = Symbol('create mode')
export function useDialogIdCU<Payload = void>(){



    const [id, setId] = useState<number | null | symbol>(null)
    const [payload, setPayload] = useState<Payload | null>(null)
    const openEdit = useCallback((id:number,payload?: Payload)=>{setId(id); payload && setPayload(payload)},[])
    const openCreate = useCallback((payload?: Payload)=>{setId(createSym) ; payload && setPayload(payload)},[])
    const hide = useCallback(() => { setId(null);setPayload(null) }, [])


    return {
        isOpen:!isNil(id),
        id,
        openEdit,
        openCreate,
        hide,
        payload,
        mode:id === createSym ? 'create' : 'edit',
    } 
}


export type DialogAwaitIO<IO extends {payload:any, outcome:any}> = IO
export interface DialogAwaitProps<IO extends {payload:any, outcome:any}, DialogStatus extends string = "success" | "cancel"> {
    // open: (payload:IO['payload'] )=>void
    hide: (status:DialogStatus,outcome?:IO['outcome'])=>any
    payload: IO['payload'] | null
}
export function useDialogAwait<IO extends {payload:any, outcome:any},DialogStatus extends string = "success" | "cancel">(){


    const resolveRef = useRef<(...args:any[])=>void>()
    const [isOpen, setOpened] = useState(false)
    const [payload, setPayload] = useState<IO['payload'] | null>(null)

    const open = useCallback((payload:IO['payload'])=>{
        setOpened(true) 
        setPayload(payload)
        const promise = new Promise<{status:DialogStatus,outcome?:IO['outcome']}>((_resolve, _reject) => { resolveRef.current = _resolve })
        return promise
    },[])
    const hide = useCallback((status:DialogStatus,outcome?:IO['outcome'])=>{
        const resolve =  resolveRef.current
        resolve!({outcome, status})
        setOpened(false)
        setPayload(null)
    },[resolveRef.current])


    return {
        isOpen,
        open,
        hide,
        payload,
        status
    }
}