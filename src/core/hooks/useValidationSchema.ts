import * as Yup from 'yup'
import {useMemo,DependencyList} from 'react'

export function useValidationSchema(schemaFactory:(yup:typeof Yup)=>Yup.AnySchema,deps:DependencyList){
    return useMemo(()=>schemaFactory(Yup),deps)
}