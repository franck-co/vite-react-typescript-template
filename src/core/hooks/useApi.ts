import { useCallback, useEffect, useState } from 'react'

export const apiStatus = {
    LOADING: 'LOADING',
    SUCCESS: 'SUCCESS',
    ERROR: 'ERROR'
  } as const
  
  export function useApi<D>(asyncRequest:()=>Promise<D>, deps:React.DependencyList,initial:any = null):[status:keyof typeof apiStatus,data:D, helpers:{refresh:()=>void, error:any}] {
    const [hookState, setHookState] = useState<{ status: keyof typeof apiStatus, data: D, error: any }>({
      status: apiStatus.LOADING,
      error: null,
      data: initial
    })
  
    const refresh = useCallback(()=>{
      const setPartData = (partialData:any) => {
        setHookState(hookState => (hookState = { ...hookState, ...partialData }))
      }
  
      setPartData({
        status: apiStatus.LOADING
      })

      asyncRequest()
        .then(data => {
          setPartData({
            status: apiStatus.SUCCESS,
            data
          })
        })
        .catch((err) => {
          setPartData({
            status: apiStatus.ERROR,
            error: err
          })
        })
    }, deps)
 

    useEffect(() => {
      refresh()
    }, deps)
  
    return [hookState.status,hookState.data, {error:hookState.error, refresh: refresh}]
  }

