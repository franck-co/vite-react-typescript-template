import { Route, Routes, Navigate, useNavigate } from 'react-router-dom'

import { HomePage } from 'pages/home-page/HomePage';
import { PlaygroundPage } from 'pages/playground-page/PlaygroundPage';

export function App(){

  return (
        <Routes>

          <Route path="/*">
            <Route path="" element={<Navigate replace to="home" />}/>
            <Route path="home/*" element={<HomePage/>} />
            <Route path="playground/*" element={<PlaygroundPage/>} />

          </Route>

          <Route path='*' element={<div>404 : not found (react router)</div>} />
        
        </Routes>
  )

}

