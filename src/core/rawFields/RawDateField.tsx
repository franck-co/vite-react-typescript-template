// import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
// import LuxonUtils from '@date-io/luxon'
import { DateTime } from "luxon";
import { FormControl, FormHelperText } from '@mui/material';
import { useState } from 'react';

import { TextField } from '@mui/material';

import AdapterLuxon from '@mui/lab/AdapterLuxon';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker, { DatePickerProps } from '@mui/lab/DatePicker';
import { RawTextField, RawTextFieldProps } from './RawTextField';


export type RawDateFieldProps = Omit<DatePickerProps, 'value' | 'onChange' | 'renderInput'> & {
  value: string | null
  onChange: (dayDateString: string | null) => void
  /** default to 'EEE d MMM yyyy' */
  displayFormat?: string
}
  & Pick<RawTextFieldProps, 'necessary' | 'missing' | 'size' | 'fullWidth' | 'helperText' | 'placeholder' | 'variant' | 'error' | 'powerAction'>

export function RawDateField(props: RawDateFieldProps) {

  const { size, fullWidth, helperText, placeholder = "01/01/2001", variant, error, necessary, missing, powerAction, ...datePickerProps } = props
  const textFieldProps = { size, fullWidth, helperText, placeholder, variant, error, necessary, missing, powerAction }
  //@ts-ignore
  Object.keys(textFieldProps).forEach((key:keyof typeof textFieldProps ) => textFieldProps[key] === undefined ? delete textFieldProps[key] : {});

  class LocalizedUtils extends AdapterLuxon {
    locale = "fr"
    getDatePickerHeaderText(dt: DateTime) {
      return dt.setLocale('fr').toFormat(props.displayFormat || "EEE d MMM yyyy")
    }
  }

  const [internalHelperText, setInternalHelperText] = useState('')


  return (
    <FormControl error={!!internalHelperText || props.error}>

      <LocalizationProvider dateAdapter={LocalizedUtils}>
        <DatePicker
          {...datePickerProps}
          value={props.value === null ? null : DateTime.fromISO(props.value)}

          // error={!!internalHelperText || props.error}
          InputProps={{ ...textFieldProps, error: !!internalHelperText || props.error, value: props.value }}
          renderInput={(props) => {
            

            //disable auto filling input with today when null
            const inputValue = (props.inputProps?.value === DateTime.now().toFormat(datePickerProps.inputFormat ?? "dd/MM/yyyy") && !props.InputProps?.value) ? '' : props.inputProps?.value;
            if (!props.inputProps) props.inputProps = {} as any
            props.inputProps!.value = inputValue

            return <RawTextField
              {...textFieldProps}
              {...props}
              error={props.InputProps?.error}
              // 
              helperText={null}

              value={inputValue} //(for the inner necessary check)
              inputProps={props.inputProps}
            />
          }}
          //@ts-ignore
          onChange={(dt: DateTime, inputText) => {
            setInternalHelperText(dt?.invalidReason ? 'Invalide' : '')
            props.onChange((dt === null || !dt?.isValid) ? null : dt?.toISO() ?? null)
          }
          }

          clearable={datePickerProps.clearable ?? true}
          inputFormat={datePickerProps.inputFormat ?? "dd/MM/yyyy"}
          clearText={datePickerProps.clearText ?? "r-à-zéro"}
          cancelText={datePickerProps.cancelText ?? "annuler"}
        />

      </LocalizationProvider>
      {(internalHelperText || props.helperText) && <FormHelperText>{internalHelperText || props.helperText}</FormHelperText>}
    </FormControl>
  );
}

          //the helper text is set internally if input is not a valid date. If we don't provide an helper text though props, we let it uncontrolled
