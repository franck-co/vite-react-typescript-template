import { IconButton, TextField, TextFieldProps } from '@mui/material'
import BoltIcon from '@mui/icons-material/Bolt';
import { withGrid,WithGridProps } from 'core/hocs';

const cancel = Symbol('cancel')
type Cancel = typeof cancel

export type RawTextFieldProps = WithGridProps<Omit<TextFieldProps,'variant'> & {
    necessary?: boolean,
    missing?: boolean,
    powerAction?: (textFieldProps: RawTextFieldProps) => any
    variant?:'outlined'|'standard'|'filled'
    transformOutput?:(value:string|null,cancel:Cancel)=>string | null | Cancel
    transformInput?:(value:string|null)=>string | null
}>
export const RawTextField = withGrid((props: RawTextFieldProps) => {
    const { necessary, missing, powerAction, variant,transformInput, transformOutput,...textFieldProps } = props

    if(transformInput) textFieldProps.value  = transformInput(textFieldProps.value  as string | null )
    if(textFieldProps.value === null) textFieldProps.value = ''

    textFieldProps.size = textFieldProps.size ?? 'small'

    const isMissing = missing ?? (necessary && (textFieldProps.value === undefined || textFieldProps.value === null || textFieldProps.value === ''))

    if (!textFieldProps.error && isMissing) {
        textFieldProps.label = `⚠️ ${textFieldProps.label}`
        textFieldProps.helperText = textFieldProps.helperText ? `${textFieldProps.helperText} - Nécessaire` : 'Nécessaire'
    }



    //convert null to "" and "" to null
    const oldOnChange = textFieldProps.onChange
    textFieldProps.onChange = undefined
    
    textFieldProps.onChange=(e)=>{

        let value:string | null
        if(e.target.value === ""){
            value = null
        }else{
          value = e.target.value
        }

        if(transformOutput) {
            const transformed = transformOutput(value,cancel)
            if(transformed === cancel) return
            else value = transformed
        }

        const event = { ...new Event('change'), ...e, target: { ...e.target, value: value } }
        oldOnChange && oldOnChange(event as any)

    }

    if (powerAction) {
        textFieldProps.InputProps = textFieldProps.InputProps ?? {}

        if (textFieldProps.InputProps.endAdornment) {
            textFieldProps.InputProps.endAdornment = (
                <>
                    {textFieldProps.InputProps.endAdornment}
                    <IconButton onClick={()=>powerAction(props)} edge="end" children={<BoltIcon />} disabled={textFieldProps.disabled}/>
                </>
            )
        } else {
            textFieldProps.InputProps.endAdornment = <IconButton onClick={()=>powerAction(props)} edge="end" children={<BoltIcon />} />
        }
    }
    return <TextField variant={variant} {...textFieldProps} sx={{
        writingMode: 'initial',
        ...(textFieldProps.error && {
            '& label, & label.Mui-focused': {
                color: theme => theme.palette.error.light,
            },
        }),
        ...(isMissing && !textFieldProps.error && {
            // '& label:after':{
            //   content:'" - Nécessaire !"'
            // },
            '& label, & label.Mui-focused,& .MuiFormHelperText-root': {
                color: theme => theme.palette.warning.light,
            },
            '& .MuiInput-underline:after': {
                borderBottomColor: theme => theme.palette.warning.light,
            },
            '& .MuiOutlinedInput-root': {
                '& fieldset': {
                    borderColor: theme => theme.palette.warning.light,
                },
                '&:hover fieldset': {
                    borderColor: theme => theme.palette.warning.light,
                },
                '&.Mui-focused fieldset': {
                    borderColor: theme => theme.palette.warning.light,
                },
            },
        })
    }} />
})
