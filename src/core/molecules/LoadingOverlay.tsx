import { Backdrop, CircularProgress, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { useState } from 'react';

export function LoadingOverlay() {

  const [open, setOpen] = useState(true)
  const [clickCount, setClickCount] = useState(0)

  const handleClick = () => {
    setClickCount(clickCount => clickCount + 1)
    if (clickCount + 1 >= 5) {
      setOpen(false)
    }
  }

  return (<Backdrop open={open} style={{ zIndex: 4000, color: '#fff', backgroundColor: 'rgba(0,0,0,0.5)' }} onClick={handleClick} >
    <Box position="relative">
      <Box position="absolute"> <CircularProgress color="inherit" /></Box>
      {!!clickCount && <Box position="absolute" top="10px" left="15px"><Typography sx={{userSelect:"none"}}>{clickCount}</Typography></Box>}
    </Box>
  </Backdrop>)
}
