import { ConditionnalTooltip, ConditionnalTooltipProps } from '../atoms/ConditionnalTooltip'

export type WithTooltipProps<P extends object> = P & {
    tooltipTitle?: ConditionnalTooltipProps['title']
    tooltipPlacement?: ConditionnalTooltipProps['placement']
    tooltipEnabled?: ConditionnalTooltipProps['enabled']
    TooltipProps?: ConditionnalTooltipProps
}

export function withTooltip<P extends object>(Component: React.ComponentType<P>): React.ComponentType<WithTooltipProps<P>> {
    return function (props) {

        const { tooltipEnabled, tooltipPlacement, tooltipTitle, TooltipProps, ...componentProps } = props

        if (tooltipEnabled !== undefined || tooltipPlacement !== undefined || tooltipTitle !== undefined || TooltipProps !== undefined) {
            const { tooltipEnabled, tooltipPlacement, tooltipTitle, TooltipProps, ...componentProps } = props
            return (
                <ConditionnalTooltip {...{ ...TooltipProps, title: tooltipTitle, placement: tooltipPlacement, enabled: tooltipEnabled }as any} >
                    <Component {...(componentProps) as any} />
                </ConditionnalTooltip>)
        }
        else {
            const { ...componentProps } = props
            return <Component {...(componentProps) as any} />
        }
    }
}