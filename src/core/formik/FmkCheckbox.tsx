import { RawRadioGroupProps, RawRadioGroup} from 'core/rawFields'
import { withGrid, WithGridProps } from 'core/hocs'
import { useField, useFormikContext } from 'formik'
import React from 'react'


// export interface FmkRawCheckboxProps<OptionData extends object> extends Omit<RawCheckboxProps<OptionData>, 'value' | 'onChange'| 'error'> { name: string, validateImmediately?:boolean,  }


// const FmkRawCheckboxUntyped = withGrid(<OptionData extends object>(props: FmkRawCheckboxProps<OptionData>) => {

//     const { name, validateImmediately,...otherProps } = props
//     const [field, meta, {setValue,setTouched}] = useField<string>(name)
//     const { isSubmitting } = useFormikContext()

//     const handleChange = (newValue:any,optionData:any)=>{
//         setTouched(true,false)
//         setValue(newValue,true)
//     }

//     const defaultProps: Partial<RawCheckboxProps<OptionData>> = {
//         size:'small',
//         onChange: handleChange
//     }

//     const config = {
//         ...field, //props from formik to field (value, onChange, onBlur, name, multiple, checked )
//         ...defaultProps,
//         ...otherProps,
//     }
//     if (meta &&  (meta.touched || validateImmediately) && meta.error) {
//         config.error = true
//         config.helperText = meta.error
//     }

//     config.disabled = config.disabled ?? isSubmitting

//     return (<RawCheckbox {...config} />)

// })
