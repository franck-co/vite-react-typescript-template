

Boutons genre
```tsx
<FmkRadioGroup
    legend='Genre'
    name="gender"
    variant='button'
    reversible
    grid xs
    options={[{ key: "M", label: 'M.' }, { key: 'F', label: 'Mme' }]}
    configure={(option) => ({
    label: option.label,
    value: option.key,
    disabled: false
    })}
    necessary
/>

```                            


Liste de checkboxes à choix multiple
```tsx
<FmkRadioGroup
    legend='Genre'
    name="MainCity"
    variant='checkbox'
    orientation='vertical'
    multiple
    reversible
    grid xs
    options={mainCities}
    configure={(option) => ({
        label: option.name,
        value: option.id,
        disabled: option.id > 5
    })}
    necessary
/>

```

Liste de villes en freeSolo
```tsx
<FmkRawComboBox
    name="MainCity"
    multiple
    options={mainCities}
    checkboxes
    configure={(option) => ({
    label: option.name,
    value: option.id,
    disabled: option.id > 5
    })}
    necessary
    label="test"
    grid xs
/>

```




