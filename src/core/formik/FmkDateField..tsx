import { TextFieldProps, TextField, Grid, alertTitleClasses } from '@mui/material'
import { withGrid } from 'core/hocs'
import { RawDateField,RawDateFieldProps } from 'core/rawFields/RawDateField'
import { useField, useFormikContext } from 'formik'
import { isString } from 'lodash'
import React from 'react'


export interface FmkDateFieldProps extends Omit<RawDateFieldProps, 'value' | 'onChange'| 'error'> { name: string , validateImmediately?:boolean}


export const FmkDateField = withGrid((props: FmkDateFieldProps) => {

    const { name,validateImmediately,...otherProps } = props
    const [field, meta,{setValue,setTouched}] = useField<string | null>(name)
    const { isSubmitting } = useFormikContext()

    const defaultProps: Partial<RawDateFieldProps> = {
        fullWidth: true,
        variant: 'outlined',
        size:'small'
    }

    const config = {
        ...field, //props from formik to field (value, onChange, onBlur, name, multiple, checked )
        ...defaultProps,
        ...otherProps,
    }

    if (meta && (meta.touched || validateImmediately) && meta.error) {
        config.error = true
        config.helperText = meta.error
    }

    config.disabled = config.disabled ?? isSubmitting

    return (<RawDateField 
        {...config}
        onChange={(dateString=>{setValue(dateString);setTouched(true,false)})}
         />)

})