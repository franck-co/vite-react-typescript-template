import { Typography } from '@mui/material';
import { Field, useField, useFormikContext } from 'formik';
import { ElementType, ReactNode } from 'react';

export function FmkErrorMessage({ name, awaitTouched = false,render:Render }: { name: string, awaitTouched?: boolean, render?:({errorText}:{errorText:string})=>React.ReactElement }){

    const [field,meta] = useField(name)
    
    const error = meta.error
    const touch = meta.touched
    const errorText = (touch ||!awaitTouched) && error && typeof error === 'string' ? error : null 
    if(!errorText) return null;

    if(Render){
        return <Render errorText={errorText} />
    }else{
        return <Typography variant='caption' sx={{color:theme=>theme.palette.error.main}}>{errorText}</Typography>
    }
}